const fs = require('fs')

const error = new Error("No such file or directory is found.")

const readingLipsumFile = (callBackFuntion)=>{
    fs.readFile('../textFiles/lipsum.txt', (err,data) => {
        err ? callBackFuntion(error.message) : callBackFuntion(data.toString())
    })
}

const convertLipsumToUpperCase = (callBackFuntion)=>{
    fs.readFile('../textFiles/lipsum.txt',(err, data)=>{
        if (err) {
            callBackFuntion(error.message)
        } else {
            const upperCaseFileContent = data.toString().toUpperCase()
            fs.writeFile('../textFiles/upperCaseContent.txt', upperCaseFileContent, (err) => {
                if (err) {
                    callBackFuntion(`While writing to a file: ${error.message}`)
                } else {
                    callBackFuntion("Text is converted successfully to the upper case.")
                    fs.appendFile('../textFiles/filenames.txt', "upperCaseContent.txt " ,(err)=>{
                        if (err) {
                            callBackFuntion(`While appending to a File: ${error.message}`)
                        } else {
                            callBackFuntion("File name is added successfully to the filenames.txt file.")
                        }
                    })
                    
                } 
            })
        }
    })
}

const convertLipsumToLowerCase=(callBackFunction)=>{
    fs.readFile('../textFiles/upperCaseContent.txt',(err, data)=>{
        if (err) {
            callBackFunction(error.message)
        } else {
            const lowerCaseFileContent = data.toString().toLowerCase().split(".").join("\n")
            fs.writeFile('../textFiles/lowerCaseLipsum.txt', lowerCaseFileContent, (err) => {
                if (err) {
                    callBackFunction(`While writing to a file: ${error.message}`)
                } else {
                    callBackFunction("Text is converted to the lower case and splitted into sentences.")
                    fs.appendFile("../textFiles/filenames.txt","lowerCaseLipsum.txt ",(err)=>{
                        if (err) {
                            callBackFunction(`While appending to a file: ${error.message}`)
                        } else {
                            callBackFunction("File name is added successfully to the filenames.txt file.")
                        }
                    })
                }
            })
        }
    })
}

const sortTheLowerCaseLipsum = (callBackFunction)=>{
    fs.readFile('../textFiles/lowerCaseLipsum.txt',(err, data)=>{
        if (err) {
            callBackFunction(`While reading a file: ${error.message}`)
        } else {
            const sortedContent = data.toString().split("\n").sort().join("\n")
            fs.writeFile('../textFiles/sortedLipsum.txt', sortedContent , (err) => {
                if (err) {
                    callBackFunction(`While writing to a file: ${error.message}`)
                } else {
                    callBackFunction("Text in the file is sorted.")
                    fs.appendFile("../textFiles/filenames.txt","sortedLipsum.txt ",(err)=>{
                        if (err) {
                            callBackFunction(`While appending to a file: ${error.message}`)
                        } else {
                            callBackFunction("File name is added successfully to the filenames.txt file.")
                        }
                    })
                }
            })
        }
    })
}

const deleteFiles = (callBackFunction)=>{
    fs.readdir('../textFiles', (err, files) => {
        if (err) {
            callBackFunction(`While reading a directory: ${error.message}`)
        } else {
            fs.readFile('../textFiles/filenames.txt',(err,data)=>{
                if (err) {
                    callBackFunction(`While reading a file: ${error.message}`)
                } else {
                    const files = data.toString().trim().split(" ")
                    files.forEach((filename)=>{
                        if (files.includes(filename)) {
                            fs.unlink(`../textFiles/${filename}`,(err) => {
                                if (err){
                                    callBackFunction(`While deleting a file: ${filename} ${error.message}`)
                                } else {
                                    callBackFunction(`${filename} is deleted successfully.`)
                                }
                            })
                        } else {
                            callBackFunction(`File ${filename} you want to delete is not exists`)
                        }
                    })
                }
            })
        }
    })
}

const problem2 = (callBackFuntion)=>{
    setTimeout(()=>{
           readingLipsumFile(callBackFuntion)
    }, 1000)

    setTimeout(()=>{
        convertLipsumToUpperCase(callBackFuntion)
    }, 2000)

    setTimeout(()=>{
        convertLipsumToLowerCase(callBackFuntion)
    }, 3000)

    setTimeout(() => {
        sortTheLowerCaseLipsum(callBackFuntion)
    }, 4000);

    setTimeout(()=>{
       deleteFiles(callBackFuntion)
    },5000)
}


module.exports = problem2
