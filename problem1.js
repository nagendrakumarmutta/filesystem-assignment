const fs = require('fs')
const problem1 = () => {
    fs.mkdir('../JSONfiles', (err) => {
        err ? console.log("File Already exists") : console.log("jsonFiles directory is created.")

        fs.writeFile('../JSONfiles/text.json', JSON.stringify({"node":"Hello Node JS"}),(err)=>{
            err ? console.log(err) : console.log("text.json file is created.")
        })
    
        fs.writeFile('../JSONfiles/text1.json', JSON.stringify({"node":"Hello Node JS"}),(err)=>{
            err ? console.log(err) : console.log("text1.json file is created.")
        })
    
        fs.writeFile('../JSONfiles/text2.json', JSON.stringify({"node":"Hello Node JS"}),(err)=>{
            err ? console.log(err) : console.log("text2.json file is created.")
        })
    });

    setTimeout(()=>{
        fs.readdir('../JSONfiles',(err,files) => {
        if (err) {
            console.log("No such file or directory is found")
        } else {
            files.forEach((fileName)=> {
                console.log(fileName)
                fs.unlink(`../JSONfiles/${fileName}`,(err) => {
                    if (err) {
                        console.log("Not Found: No such file exists")
                    } else {
                        console.log(`Deleted filename is ${fileName}`)
                    }
                })
            })
        }
    })}, 1000)
}

module.exports = problem1
